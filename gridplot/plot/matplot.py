"""
.. moduleauthor:: Yann Maret <yann.maret@hevs.ch>

.. codeauthor:: Yann Maret <yann.maret@hevs.ch>

"""

from gridplot.connector.plotconnector import PlotListener

from gridsim.cyberphysical.core import SyncParam

from gridman.activity import ActivityListener

from matplotlib import pyplot as plt
from matplotlib import animation as anim

from matplotlib.widgets import Button

import threading
import time


class PlotOrganize(object):
    def __init__(self):
        """
        __init__(self)
        """
        super(PlotOrganize, self).__init__()

        self.series = {}
        self.early_series = {}

        self.plot_position = 1

        self.xlabel = "Time [s]"
        self.ylabel = "Value[?]"

        self.annotation = False

        self.ax = None

        self.xlimit_down = None
        self.xlimit_up = None


class MatPlot(PlotListener, SyncParam, ActivityListener):
    figure_id = 0

    @staticmethod
    def auto_inc_figure_id():
        """
        auto_inc_figure_id()
        """
        MatPlot.figure_id += 1

    def __init__(self, plot_name, start_register_time=0):
        """
        __init__(self,plot_name,start_register_time)

        :param plot_name:
        :type plot_name str
        :param start_register_time:
        """
        PlotListener.__init__(self)
        SyncParam.__init__(self)
        ActivityListener.__init__(self)

        self._current_plot_id = MatPlot.figure_id
        MatPlot.auto_inc_figure_id()

        self._plot_name = plot_name

        self._start_register_time = start_register_time
        self._x_time_data = []

        self._lock = threading.Lock()
        self._plot = []

        self._thread = None

    def plot_cont(self):
        """
        plot_cont(self)
        """
        while True:
            fig = plt.figure(self._current_plot_id)
            fig.suptitle(self._plot_name, fontsize=20)

            i = 1
            for plot in self._plot:
                plot.ax = fig.add_subplot(len(self._plot)/1, 1, i) # ,2
                i += 1

                plt.xlabel(plot.xlabel)
                plt.ylabel(plot.ylabel)

            def update(it):
                """
                update(it)

                :param it: iterator over the frame
                """
                for plot in self._plot:
                    plot.ax.clear()
                    plt.sca(plot.ax)

                    # self._lock.acquire()
                    # plot dynamic series
                    for n, y in plot.series.items():
                        x = self._x_time_data[:len(y)]
                        if len(n) == 3:
                            name = str(n[0]).split('.')[1][1:] + '_' + str(n[2]) + '_' + n[1]
                        else:
                            name = str(n[0]).split('.')[1][1:]
                        plot.ax.plot(x, y, label=name)
                        # add annotation
                        if plot.annotation:
                            for i, j in zip(x, y):
                                plot.ax.annotate(str(round(j, 2)), xy=(i, j))
                    # self._lock.release()

                    # plot static series
                    for n, data in plot.early_series.items():
                        x, y = data
                        plot.ax.plot(x, y, label=str(n))

                    plt.xlabel(plot.xlabel)
                    plt.ylabel(plot.ylabel)
                    if plot.xlimit_down is not None and plot.xlimit_up is not None:
                        plt.xlim([plot.xlimit_down, plot.xlimit_up])

                    plt.legend(loc='best', prop={'size': 8})
                    plt.grid()

            # buttonax = fig.add_axes([0.45, 0.9, 0.1, 0.075])
            # self.button = Button(buttonax, 'Update')
            # self.button.on_clicked(pause)
            a = anim.FuncAnimation(fig, update, repeat=False, interval=1000)
            plt.show()

            self.activity_state = 0
            print 'close matplotlib window: ' + self._plot_name

            while self.activity_state == 0:
                time.sleep(1)

    def create_plot_data(self, ylabel, plot_params, annotation=False, xlimit_down=None, xlimit_up=None):
        """
        create_plot_data(self,ylabel,plot_params,annotation)

        :param ylabel:
        :param plot_params:
        :param annotation:
        :return:
        """
        # add basic property like plot value on graph,...
        plot_organize = PlotOrganize()
        plot_organize.ylabel = ylabel
        plot_organize.annotation = annotation

        plot_organize.xlimit_down = xlimit_down
        plot_organize.xlimit_up = xlimit_up

        for p in plot_params:
            if p not in self.plot_params:
                self.plot_params.append(p)
            plot_organize.series[p] = []

        self._plot.append(plot_organize)

    def early_plot_data(self, plot, x, y):
        """
        early_plot_data(self,plot,x,y)

        :param plot:
        :param x:
        :param y:
        :return:
        """
        if plot < len(self._plot):
            for n, v in y.items():
                self._plot[plot].early_series[n] = (x, v)

    def start_ploting(self):
        """
        start_ploting(self)
        """
        if self._thread == None:
            self._thread = threading.Thread(target=self.plot_cont)
            self._thread.start()

    # fixme write begin
    def cyberphysical_share_time(self, time, delta_time):
        self._x_time_data.append(time)

    def notify_plot_param(self, plot_param, data):
        #print 'notify_plot_param',plot_param,data
        for plot in self._plot:
            if plot_param in plot.series.keys():  # update the current key array
                # self._lock.acquire()
                plot.series[plot_param].append(data)
                # self._lock.release()

    def on_sync_value(self, callable, write_param, data):
        print write_param, data
        for plot in self._plot:
            if write_param in plot.series.keys():  # update the current key array
                # self._lock.acquire()
                print 'add to plot',write_param,data
                plot.series[write_param].append(data)
                # self._lock.release()

    def activity_new_state(self, data):
        # restart thread if killed
        #print 'activity_new_state', data
        if 'play' in data or len(data) == 0:
            self.activity_state = 1
        if 'pause' in data:
            self.activity_state = 2


matplots = []

def start_plot():
    pass

def draw_matplot_thread():
    for matplot in matplots:
        plt.show()
