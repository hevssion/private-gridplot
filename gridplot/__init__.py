import pkg_resources  # part of setuptools
__file__
__version__ = pkg_resources.require("Gridplot")[0].version
