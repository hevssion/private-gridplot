"""
.. moduleauthor:: Yann Maret <yann.maret@hevs.ch>

.. codeauthor:: Yann Maret <yann.maret@hevs.ch>

"""

from gridsim.decorators import accepts, returns
from gridsim.cyberphysical.simulation import CyberPhysicalModuleListener


class PlotListener(object, CyberPhysicalModuleListener):
    def __init__(self):
        super(PlotListener, self).__init__()

        self.plot_params = []

    def notify_plot_param(self, plot_param, data):
        raise NotImplementedError("Pure abstract method!")


class PlotParam(object):
    def __init__(self, plot_param):
        super(PlotParam, self).__init__()

        self._listener = []
        self.plot_param = plot_param

    @accepts((1, PlotListener))
    @returns(PlotListener)
    def add_listener(self, plot):
        if plot not in self._listener:
            self._listener.append(plot)
        return plot

    def notify_plot_param(self, data):
        for l in self._listener:
            l.notify_plot_param(self.plot_param, data)


class AbstractPlotSystem:
    def __init__(self):

        self.plot = []

        self.plot_measures = []
        self.plot_measures_write = []
        self.plot_set_point = []

    @accepts((1, PlotListener))
    @returns(PlotListener)
    def add_plot(self, plot):
        plot_params = plot.plot_params  # plot register param type
        for p in self.plot_measures:  # house paramtype
            for l in plot_params:
                # print l,p.plot_param
                # TODO check for tuple type and length conformity
                if l[0] == p.plot_param[0] and l[1] == p.plot_param[1] \
                        and l[2] == p.plot_param[2]:
                    print 'add to plot', l
                    p.add_listener(plot)

        for p in self.plot_measures_write:  # house paramtype
            for l in plot_params:
                if l[0] == p.plot_param[0] and l[1] == p.plot_param[1] \
                        and l[2] == p.plot_param[2]:
                    print 'add to plot', l
                    p.add_listener(plot)

        self.plot.append(plot)
        return plot

    def update_plot_measure(self, data):
        for pp in self.plot_measures:
            if len(data) is not 0:
                pp.notify_plot_param(data.pop(0))

    def update_plot_setpoint(self, data):
        for pp in self.plot_measures_write:
            if len(data) is not 0:
                pp.notify_plot_param(data.pop(0))
