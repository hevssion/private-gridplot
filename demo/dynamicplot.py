from gridplot.plot.matplot import MatPlot

import random
import time

mat = MatPlot(None)
mat.initialize_plot()

i=0

while(True):
    time.sleep(2)
    mat.notify_plot_param(i,random.random()*100,'a')
    mat.notify_plot_param(i,random.random()*100,'b')
    i+=1